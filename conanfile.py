from conans import ConanFile, AutoToolsBuildEnvironment, tools


class CairoConan(ConanFile):
	name = "Cairo"
	version = "1.14.12"
	license = ["LGPL v2.1", "MPL 1.1"]
	url = "https://gitlab.com/paulbendixen/conanCairo"
	description = "Cairo library"
	settings = "os", "compiler", "build_type", "arch"
	options = {"shared": [True, False]}
	default_options = "shared=True"
	generators = "cmake"
	requires = "Pixman/0.34.0@paulbendixen/stable"

	def source(self):
		tools.get( "http://www.cairographics.org/releases/cairo-{}.tar.xz".format( self.version ) )

	def build(self):
		maker = AutoToolsBuildEnvironment( self )
		maker.configure( configure_dir="cairo-{}".format( self.version ) )
		maker.make()
		maker.install()

	def package(self):
		self.copy("*.h", dst="include", src="cairo-{}/cairo/".format( self.version ) )
		self.copy("*hello.lib", dst="lib", keep_path=False)
		self.copy("*.dll", dst="bin", keep_path=False)
		self.copy("*.so", dst="lib", keep_path=False)
		self.copy("*.dylib", dst="lib", keep_path=False)
		self.copy("*.a", dst="lib", keep_path=False)

	def package_info(self):
		self.cpp_info.libs = ["cairo"]

